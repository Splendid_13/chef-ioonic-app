import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { LoginPageModule } from "../pages/login/login.module";
import { OrderPageModule } from "../pages/order/order.module";
import { SignupPageModule } from "../pages/signup/signup.module";
import { CartPageModule } from "../pages/cart/cart.module";
import { CartPage } from "../pages/cart/cart";
import { SignupPage } from "../pages/signup/signup";
import { OrderPage } from "../pages/order/order";
import { LoginPage } from "../pages/login/login";
import { HomePageModule } from "../pages/home/home.module";

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    LoginPageModule,
    HomePageModule,
    OrderPageModule,
    SignupPageModule,
    CartPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    OrderPage,
    SignupPage,
    CartPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
