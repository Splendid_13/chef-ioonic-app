import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { LoginPage } from "../login/login";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  private todo: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder
  ) {
    this.todo = this.formBuilder.group({
      fullname: ["", Validators.required],
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SignupPage");
  }
  logForm() {
    console.log(this.todo.value);
  }
  signin() {
    this.navCtrl.push(LoginPage);
  }
}
